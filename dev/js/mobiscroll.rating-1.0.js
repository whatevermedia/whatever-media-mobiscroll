(function ($) {

    var defaults = {
        inputClass: '',
        values: 5,
        order: 'desc',
        style: 'star',
        showText: false,
        invalid: []
    }

    $.scroller.presets.rating = function (inst) {
        var s = $.extend({}, defaults, inst.settings),
            elm = $(this),
            id = this.id + '_dummy',
            l1 = $('label[for="' + this.id + '"]').attr('for', id),
            l2 = $('label[for="' + id + '"]'),
            label = s.label !== undefined ? s.label : (l2.length ? l2.text() : elm.attr('name')),
            w = [{}],
            wheel = {},
            main = {},
            input,
            style = s.style == 'grade' ? 'circle' : 'star'; // Currently supporting only two kinds of shapes

        if (inst.settings.showText === undefined && ((jQuery.isArray(inst.settings.values) && (typeof (inst.settings.values[0]) === 'string')) || $(this).is('select'))) {
            s.showText = true;
        }
        //
        // Initialization
        //

        input = null;
        if (elm.is('select')) {
            var n = $('option', elm).length;
            s.values = {};
            $('option', elm).each(function (index) { // Create text from the select options
                s.values[$(this).val()] = $(this).text();
            });
            // Create a textinput before the select, which will hold the selected value
            $('#' + id).remove();
        }

        var N;
        if (jQuery.isPlainObject(s.values)) { // values: {3:'bad', 5:'good', 4:'ok'}
            N = objectMax(s.values);
            if (s.order == 'desc') {
                var propa = [];
                for (var prop in s.values) {
                    propa.push(prop);
                }
                for (var nth = 1; nth <= propa.length; nth++) {
                    var starC = parseInt(propa[propa.length - nth]);
                    createNewEntry(starC, nth, propa[nth - 1]); //N - nth + 1
                    main['_' + nth] = s.values[propa[nth - 1]];
                }
            } else {
                var nth = 1
                for (var prop in s.values) {
                    var starC = parseInt(prop);
                    createNewEntry(starC, nth, prop);
                    main['_' + nth] = s.values[prop];
                    nth++;
                }
            }
        } else
            if (jQuery.isArray(s.values)) {
                if (typeof s.values[0] === 'string') { // values: ["bad", "good"] 
                    N = s.values.length;
                    sLength = s.values.length;
                    if (s.order == 'desc') {
                        for (var nth = 1; nth <= sLength; nth++) {
                            createNewEntry(sLength - nth + 1, nth, nth);
                            main['_' + nth] = s.values[nth - 1];
                        }
                    }
                    else {
                        for (var nth = 1; nth <= sLength; nth++) {
                            createNewEntry(nth, nth, nth);
                            main['_' + nth] = s.values[nth - 1];
                        }
                    }

                }
                else { //  values: [3, 4, 5]
                    N = arrayMax(s.values);
                    sLength = s.values.length;
                    if (s.order == 'desc') {
                        for (var nth = 1; nth <= sLength; nth++) {
                            createNewEntry(s.values[sLength - nth], nth, sLength - nth + 1);
                            main['_' + nth] = s.values[sLength - nth];
                        }
                    }
                    else {
                        for (var nth = 1; nth <= sLength; nth++) {
                            createNewEntry(s.values[nth - 1], nth, nth);
                            main['_' + nth] = s.values[nth - 1];
                        }
                    }
                }
            }
            else { // values: 6
                N = s.values;
                if (s.order == 'desc') { // Create the scroll entries for the ratings
                    for (var nth = 1; nth <= N; nth++) {
                        createNewEntry(N - nth + 1, nth, nth);
                        main['_' + nth] = N - nth + 1;
                    }
                }
                else {
                    for (nth = 1; nth <= N; nth++) {
                        createNewEntry(nth, nth, nth);
                        main['_' + nth] = nth;
                    }
                }
            }
        if (elm.is('select'))
            input = $('<input type="text" id="' + id + '" value="' + main['_' + (elm.val() ? elm.val() : s.order == 'asc' ? "1" : N)] + '" class="' + s.inputClass + '" readonly />').insertBefore(elm);
        w[0][label] = wheel;
        if (s.showOnFocus && input)
            input.focus(function () { inst.show() });
        if (elm.is('select'))
            elm.hide().closest('.ui-field-contain').trigger('create');

        return {
            //width: 200,
            height: 40,
            wheels: w,
            headerText: false,
            formatResult: function (d) {
                return main[d[0]];
            },
            parseValue: function () {
                if (elm.val())
                    return ['_' + elm.val()];
                else
                    return ['_1'];
            },
            validate: function (dw) {
                $.each(s.invalid, function (i, v) {
                    $('li[data-val="_' + v + '"]', dw).removeClass('dw-v');
                });
            },
            onSelect: function (v, inst) {
                if (input) {
                    input.val(v);
                    elm.val(inst.values[0]).trigger('change');
                }
            },
            onChange: function (v, inst) {
                if (s.display == 'inline') {
                    if (input) {
                        input.val(v);
                        elm.val(inst.temp[0]).trigger('change');
                    }
                }
            },
            onClose: function () {
                if (input)
                    input.blur();
            }
        }

        function objectMax(obj) {
            var max = null;
            for (var prop in obj) {
                var i = parseInt(prop);
                if (max == null) {
                    max = i;
                }
                if (max < i)
                    max = i;
            }
            return max;
        }
        function arrayMax(array) {
            var max = array[0];
            for (var i = 1; i < array.length; i++)
                if (max < array[i])
                    max = array[i];
            return max;
        }

        // Create the n-th entry on the wheel
        function createNewEntry(starNumber, nthEntry, nameIndex) {
            var irate = '';
            for (var i = 1; i < starNumber + 1; i++) {
                irate += filledShape(style, i);
            }
            for (var i = starNumber + 1; i <= N; i++) {
                irate += unfilledShape(style);
            }
            irate += s.showText ? nameMarkup(nameIndex) : "";
            wheel['_' + nthEntry] = irate;
        }

        // Create the markup of a filled shape
        function filledShape(style, i) {
            return '<div class="rating-star-cont"><div class="rating-' + style + ' rating-filled-' + style + '">' + (style == 'circle' ? i : '') + '</div></div>';
        }

        // Create the markup of an unfilled shape
        function unfilledShape(style) {
            return '<div class="rating-star-cont"><div class="rating-' + style + ' rating-unfilled-' + style + '"></div></div>';
        }

        // Create the markup of the text that appears after the shapes
        function nameMarkup(j) {
            var nm = '';
            if (s.showText) {
                if (jQuery.isPlainObject(s.values))
                    nm = s.values[j];
                else if (jQuery.isArray(s.values))
                    nm = s.values[j - 1];
                else
                    nm = j + '';
            }
            return '<div class="rating-txt">' + nm + '</div>'; //(s.showText && s.text ? s.text[j - 1] : jQuery.isArray(s.values) ? s.values[j - 1] : j)
        }
    }
})(jQuery);
